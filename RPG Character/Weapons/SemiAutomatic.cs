﻿using System;
namespace RPG_Character
{
    public class SemiAutomatic : Weapon
    {
        public SemiAutomatic(string name)
            : base(name)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Theif);
        }

        public SemiAutomatic(string name, string nickname)
            : base(name, nickname)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Theif);
        }

        public SemiAutomatic()
            : base("Uzi", "Lil Uzi Vert")
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Theif);
        }
    }
}
