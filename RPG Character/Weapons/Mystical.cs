﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    public class Mystical : Weapon
    {
        public Mystical(string name)
            : base(name)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }

        public Mystical(string name, string nickname)
            : base(name, nickname)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }

        public Mystical()
            : base("Mystical Want", "Hairy Spotter")
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }
    }
}
