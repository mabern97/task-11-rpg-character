﻿using System;
using System.ComponentModel.Design;
using System.Reflection;
using System.Text;

namespace RPG_Character
{
    [Flags]
    public enum WeaponCharacterFlags
    {
        Wizard = 1,
        Warrior = 2,
        Theif = 4,
        Mage = 8,
        Gangster = 16,
    }

    /* 
     * Weapon inherits an 'Entity' abstract class
     * The Entity class provides every entity with a default 'Name'
     * property.
    */
    public abstract class Weapon : Entity
    {
        public string Nickname { get; set; }

        // Weapon Properties
        public int FireRate { get; set; }
        public int DamagePerHit { get; set; }
        public int Range { get; set; }
        public int ClipSize { get; set; }
        public int MaxAmmo { get; set; }

        public WeaponCharacterFlags compatibleCharacters { get; private set; }

        // List of all defined weapon types
        public static Type[] DefinedWeapons = {
            typeof(Shotgun),
            typeof(SemiAutomatic),
            typeof(Pistol),
            typeof(Rifle),
            typeof(Mystical),
        };

        public Weapon(string name)
            :base(name)
        {
            Nickname = Name;
            Setup();
        }

        public Weapon(string name, string nickname)
            :base(name)
        {
            Nickname = nickname;
            Setup();
        }

        private void Setup()
        {
            FireRate = 15;
            DamagePerHit = 20;
            Range = 5;
            ClipSize = 20;
            MaxAmmo = 150;
        }

        public string GetWeaponType()
        {
            return GetType().Name;
        }

        public void AddCompatibleCharacter(WeaponCharacterFlags flags)
        {
            compatibleCharacters |= flags;
        }

        /*
         * Check if 2 weapon objects are alike or not
         */
        public bool Equals(Weapon obj)
        {
            bool nameMatch = Name.Equals(obj.Name);
            bool nickMatch = Nickname.Equals(obj.Nickname);
            bool fireRateMatch = FireRate.Equals(obj.FireRate);
            bool dmgMatch = DamagePerHit.Equals(obj.DamagePerHit);
            bool rangeMatch = Range.Equals(obj.Range);
            bool clipMatch = ClipSize.Equals(obj.ClipSize);
            bool maxAmmoMatch = MaxAmmo.Equals(obj.MaxAmmo);

            return nameMatch && nickMatch && fireRateMatch && dmgMatch && rangeMatch && clipMatch && maxAmmoMatch;
        }

        /*
         * Create a cloned instance of an existing weapon.
         */
        public Weapon Clone()
        {
            Weapon clone = Activator.CreateInstance(GetType(), Name, Nickname) as Weapon;

            clone.FireRate = FireRate;
            clone.DamagePerHit = DamagePerHit;
            clone.Range = Range;
            clone.ClipSize = ClipSize;
            clone.MaxAmmo = MaxAmmo;

            return clone;
        }

        /*
         * Returns a string containing details regarding the weapon
         */
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Weapon - Type: {GetType().Name} (\n");
            sb.Append($"\t Name: {Name}\n");
            sb.Append($"\t Nickname: {Nickname}\n");
            sb.Append($"\t Fire Rate: {FireRate}\n");
            sb.Append($"\t Damage Per Hit: {DamagePerHit}\n");
            sb.Append($"\t Range: {Range}\n");
            sb.Append($"\t Clip Size: {ClipSize}\n");
            sb.Append($"\t Maximum Ammo: {MaxAmmo}\n");
            //sb.Append($"\t Weapons: {weaponList}\n");
            sb.Append("\n)\n");

            return sb.ToString();
        }
    }
}
