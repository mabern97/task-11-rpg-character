﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    class Rifle : Weapon
    {
        public Rifle(string name)
            : base(name)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Warrior | WeaponCharacterFlags.Theif);
        }
        public Rifle(string name, string nickname)
            :base(name, nickname)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Warrior | WeaponCharacterFlags.Theif);
        }

        public Rifle()
            : base("Country Rifle", "Watermelon Popper")
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Warrior | WeaponCharacterFlags.Theif);
        }
    }
}
