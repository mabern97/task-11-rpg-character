﻿using System;
namespace RPG_Character
{
    public class Pistol : Weapon
    {
        public Pistol(string name)
            : base(name)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }

        public Pistol(string name, string nickname)
            : base(name, nickname)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }

        public Pistol()
            : base("Glock 19", "Happy Stick")
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }
    }
}
