﻿using System;
namespace RPG_Character
{
    public class Shotgun : Weapon
    {
        public Shotgun(string name)
            : base(name)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }

        public Shotgun(string name, string nickname)
            : base(name, nickname)
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }

        public Shotgun()
            : base("Pump Shotgun", "The Kurt Cobain")
        {
            AddCompatibleCharacter(WeaponCharacterFlags.Wizard);
        }
    }
}
