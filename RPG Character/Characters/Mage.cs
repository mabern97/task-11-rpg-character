﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    class Mage : Wizard
    {
        public Mage(string name)
            : base(name)
        {
        }

        public override bool IsWeaponCompatible(Weapon weapon)
        {
            bool compatible = false;

            if ((weapon.compatibleCharacters & WeaponCharacterFlags.Wizard) == WeaponCharacterFlags.Wizard)
                compatible = true;
            else if ((weapon.compatibleCharacters & WeaponCharacterFlags.Mage) == WeaponCharacterFlags.Mage)
                compatible = true;

            return compatible;
        }
    }
}
