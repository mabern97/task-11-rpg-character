﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    public interface IEntity
    {
        double Move();
        double Attack();
    }
}
