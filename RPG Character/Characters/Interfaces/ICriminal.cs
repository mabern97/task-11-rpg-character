﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    public interface ICriminal
    {
        double Steal();

        // Burgle -> Steal something
        double Burgle();
    }
}
