﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace RPG_Character
{
    /* 
     * Character inherits an 'Entity' abstract class
     * The Entity class provides every entity with a default 'Name'
     * property.
    */
    public abstract class Character : Entity, IEntity
    {
        // List of all defined character types
        public static Type[] Characters
        {
            get
            {
                return new Type[] {
                    typeof(Thief),
                    typeof(Warrior),
                    typeof(Wizard),
                    typeof(Mage),
                    typeof(Gangster),
                };
            }
        }

        // Main Properties all characters have
        public int Health { get; set; } // HP
        public int Armor { get; set; } // ArmorRating
        public int Energy { get; set; } // Energy
        public int Mana { get; set; } // Mana

        private List<Weapon> weapons;

        public Character(string name)
            :base(name)
        {
            Setup();
        }

        // Return the character type (From the classname)
        public string GetCharacterType()
        {
            return GetType().Name;
        }

        // Assign default values for each attribute
        private void Setup()
        {
            weapons = new List<Weapon>();
            Health = 100;
            Armor = 100;
            Energy = 100;
            Mana = 100;
        }

        /*
         * Method will give the character access to the given weapons provided.
         * A check is executed checking if the Character type supports the given weapon:
         *      If it passes: The weapon is given to the character
         *      If it fails: The character is not given the weapon
         */
        public void GiveWeapon(params Weapon[] weapons)
        {
            foreach(Weapon weapon in weapons)
            {
                if (IsWeaponCompatible(weapon))
                {
                    this.weapons.Add(weapon);
                    Console.WriteLine($"{GetType()}) Character '{Name}' now has a '{weapon.Nickname}' in their inventory");
                } else
                {
                    Console.WriteLine($"{GetType()}) Character '{Name}' can't use the weapon '{weapon.Nickname}', they don't have the usage flag");
                }
            }
        }

        /*
         * Returns a string with all weapons the character has in their inventory
         */
        public string GetListOfWeapons()
        {
            IEnumerable<string> weaponList = from weapon in weapons
                                 select weapon.Nickname;

            return string.Join(", ", weaponList);
        }

        /* 
         * Abstract method to check if the weapon is compatible with the given character.
         * This method MUST be implemented for every new character that is defined, as logic can change.
        */
        public abstract bool IsWeaponCompatible(Weapon weapon);

        /*
         * Check if 2 character objects are alike or not
         */
        public bool Equals(Character obj)
        {
            bool nameMatch = Name.Equals(obj.Name);
            bool hpMatch = Health.Equals(obj.Health);
            bool apMatch = Armor.Equals(obj.Armor);
            bool energyMatch = Energy.Equals(obj.Energy);
            bool manaMatch = Mana.Equals(obj.Mana);

            return nameMatch && hpMatch && apMatch && energyMatch && manaMatch;
        }

        /*
         * Create a cloned instance of an existing character.
         */
        public Character Clone()
        {
            Character clone = Activator.CreateInstance(GetType(), Name) as Character;

            clone.Health = Health;
            clone.Armor = Armor;
            clone.Energy = Energy;
            clone.Mana = Mana;

            return clone;
        }

        /*
         * Returns a string containing details regarding the character
         */
        public override string ToString()
        {
            string weaponList = GetListOfWeapons();

            if (weaponList.Length == 0)
                weaponList = "No Weapons";

            StringBuilder sb = new StringBuilder();
            sb.Append($"Character - Type: {GetType().Name} (\n");
            sb.Append($"\t Name: {Name}\n");
            sb.Append($"\t Health: {Health}\n");
            sb.Append($"\t Armor: {Armor}\n");
            sb.Append($"\t Energy: {Energy}\n");
            sb.Append($"\t Mana: {Mana}\n");
            //sb.Append($"\t Weapons: {weaponList}\n");
            sb.Append("\n)\n");

            return sb.ToString();
        }

        public double Move()
        {
            Random random = new Random();
            double distance = random.NextDouble() * 100.0;

            return distance;
        }

        public double Attack()
        {
            Random random = new Random();
            double attk = random.NextDouble() * 40.0;

            return attk;
        }
    }
}
