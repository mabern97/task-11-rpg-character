﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    class Gangster : Character, ICriminal
    {
        public Gangster(string name)
            :base(name)
        {
        }

        public override bool IsWeaponCompatible(Weapon weapon)
        {
            bool compatible = false;

            if ((weapon.compatibleCharacters & WeaponCharacterFlags.Gangster) == WeaponCharacterFlags.Gangster)
                compatible = true;

            return compatible;
        }

        public double Burgle()
        {
            int items = new Random().Next(0, 30);

            return Convert.ToDouble(items);
        }

        public double Steal()
        {
            double money = new Random().NextDouble() * 3000;

            return money;
        }
    }
}
