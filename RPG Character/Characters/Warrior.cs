﻿using System;

namespace RPG_Character
{
    public class Warrior : Character
    {
        public Warrior(string name)
            :base(name)
        {
        }

        public override bool IsWeaponCompatible(Weapon weapon)
        {
            bool compatible = false;

            if ((weapon.compatibleCharacters & WeaponCharacterFlags.Warrior) == WeaponCharacterFlags.Warrior)
                compatible = true;

            return compatible;
        }
    }
}
