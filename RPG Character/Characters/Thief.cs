﻿using System;

namespace RPG_Character
{
    public class Thief : Character
    {
        public Thief(string name)
            :base(name)
        {
        }

        public override bool IsWeaponCompatible(Weapon weapon)
        {
            bool compatible = false;

            if ((weapon.compatibleCharacters & WeaponCharacterFlags.Theif) == WeaponCharacterFlags.Theif)
                compatible = true;

            return compatible;
        }
    }
}
