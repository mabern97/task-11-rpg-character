﻿using System;

namespace RPG_Character
{
    public class Wizard : Character
    {
        public Wizard(string name)
            :base(name)
        {
        }

        public override bool IsWeaponCompatible(Weapon weapon)
        {
            bool compatible = false;

            if ((weapon.compatibleCharacters & WeaponCharacterFlags.Wizard) == WeaponCharacterFlags.Wizard)
                compatible = true;

            return compatible;
        }
    }
}
