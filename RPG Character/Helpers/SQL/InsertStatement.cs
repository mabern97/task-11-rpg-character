﻿using System;
using System.Collections.Generic;

namespace RPG_Character
{
    public class InsertStatement
    {
        // The list will ALWAYS be odd since the parameter list is always even, and the data is divided
        public List<string> identifiers; // All parameters provided
        public List<object> values; // Values to all the parameters

        // The SQL query for INSERT
        public string Statement { get; private set; }

        // The number of parameters passed
        public int Count
        {
            get { return identifiers.Count; }
        }

        /*
         * SQL Insert query
        */
        public InsertStatement(string table, object[] parameters)
        {
            // If the parameter count is not even, then the data provided is invalid as the count always has to be EVEN. If not even, data is missing
            if (!SQLHelper.IsParameterCountValid(parameters))
                throw new Exception($"Invalid parameter count, got {parameters.Length} parameters. Parameter '{parameters[parameters.Length - 1]}' has no value assigned to it.");

            List<string> arguments = new List<string>(); // List of column names
            identifiers = new List<string>(); // List of prepared keys (i.e @key)
            values = new List<object>(); // List of all the variables each column will have

            for (int i = 0; i < parameters.Length; i++)
            {
                if (i % 2 == 0) // Even index = Names
                {
                    string argument = parameters[i].ToString();

                    arguments.Add(argument);
                    identifiers.Add($"@val{argument}");
                }
                else // Odd index = Values
                {
                    values.Add(parameters[i]);
                }
            }

            // Join all the arguments that we're inserting into and join all the prepared keys
            Statement = $"INSERT INTO {table} ({string.Join(", ", arguments)}) VALUES ({string.Join(", ", identifiers)})";
        }
    }
}
