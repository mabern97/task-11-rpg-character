﻿using System;
namespace RPG_Character
{
    public class UpdateStatement
    {
        public string Statement { get; private set; }

        /*
         * SQL Update query
        */
        public UpdateStatement(string table, Conditions set, Conditions where)
        {
            Statement = $"UPDATE {table}{set.Condition}{where.Condition}";
        }
    }
}
