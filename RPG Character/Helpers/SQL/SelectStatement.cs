﻿using System;
using System.Collections.Generic;

namespace RPG_Character
{
    public class SelectStatement
    {
        // The SQL query for SELECT
        public string Statement { get; private set; }

        // List of KVPairs containing a prepared key and its value
        public List<KeyValuePair<string, object>> Values { get; }

        public SelectStatement(string table, SelectColumns names, Conditions where)
        {
            string selectedNames = names.Selected;

            string whereStatement = where.Condition;
            
            Statement = $"SELECT {selectedNames} FROM {table}{whereStatement}";
            Values = where.Values;
        }
    }

    // Class containing all columns the SELECT query will use
    public class SelectColumns
    {
        public string Selected { get; }

        public SelectColumns(params string[] names)
        {
            if (names.Length == 0)
                Selected = "*";
            else
            {
                Selected = string.Join(", ", names);
            }
        }
    }

    // The whole result of the SELECT query
    public class SelectResult
    {
        public List<string> Columns { get; } // A list of all the column names
        /*
         *  Dictionary containing an index for
         *  each column (as a number (i.e 0 to N)
         *  and their respective values
         */
        private Dictionary<int, Dictionary<string, object>> Values { get; }

        // Returns the amount of columns selected
        public int ColumnCount
        {
            get { return Columns.Count; }
        }

        // Returns the number of rows collected
        public int RowCount
        {
            get { return Values.Count; }
        }

        public SelectResult()
        {
            Columns = new List<string>();
            Values = new Dictionary<int, Dictionary<string, object>>();
        }

        // Adds a column
        public void AddColumn(string name)
        {
            Columns.Add(name);
        }

        // Adds a row containing all the objects
        public void AddValue(int id, params object[] values)
        {
            Dictionary<string, object> rowValues = new Dictionary<string, object>();

            for (int i = 0; i < Columns.Count; i++)
            {
                string columnName = Columns[i];
                object columnValue = values[i];

                rowValues.Add(columnName, columnValue);
            }

            Values.Add(id, rowValues);
        }

        // Retrieve a row via the index of the row
        public Dictionary<string, object> GetRowValues(int row)
        {
            return Values[row];
        }
    }
}
