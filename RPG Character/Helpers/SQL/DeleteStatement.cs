﻿using System;
namespace RPG_Character
{
    public class DeleteStatement
    {
        public string Statement { get; private set; }

        /*
         * SQL Delete query
        */
        public DeleteStatement(string table, Conditions where)
        {
            Statement = $"DELETE FROM {table}{where.Condition}";
        }
    }
}
