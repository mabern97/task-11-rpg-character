﻿using System;
using System.Collections.Generic;

namespace RPG_Character
{
    // Supported types of Conditions the class can generate SQL queries for
    public enum ConditionType
    {
        Set,
        Where
    };

    public class Conditions
    {
        // Stores the generated SQL clauses for SET and WHERE
        public string Condition { get; }

        /* Each key is a prepared statement (i.e @keyName = val)
         * which stores the value that the key is assigned to
        */
        public List<KeyValuePair<string, object>> Values { get; }

        public Conditions(ConditionType condition, params object[] data)
        {
            Values = new List<KeyValuePair<string, object>>();

            if (data.Length == 0) // If there is no data, the clause will remain empty;
                Condition = "";
            else
            {
                // If the parameter count is not even, then the data provided is invalid as the count always has to be EVEN. If not even, data is missing
                if (!SQLHelper.IsParameterCountValid(data))
                    throw new Exception($"Invalid parameter count, got {data.Length} parameters. Parameter '{data[data.Length - 1]}' has no value assigned to it.");

                string lastName = "";
                string[] dataClause = new string[data.Length / 2]; // Since the data is even, divide the length by two since we only really need N/2 keys;

                Random r = new Random();
                for (int i = 0; i < data.Length; i++)
                {
                    int rid = r.Next(2000); // Generate a random number that the key has (to avoid having duplicate keys (A problem that can occur when executing a SET clause with a WHERE clause too))

                    object obj = data[i]; // The value of the key
                    if (i % 2 == 0) // Name
                    {
                        lastName = $"@val{rid}{obj}"; // The last key that was iterated and generated
                        dataClause[i / 2] = $"{obj} = @val{rid}{obj}"; // The regular table name and the prepared key
                    }
                    else
                    {
                        KeyValuePair<string, object> val = new KeyValuePair<string, object>(lastName, obj); // A pair containing the prepared key and the assigned value

                        Values.Add(val);
                    }
                }

                // Create the clause and join all keys together in their corrosponding order
                switch (condition)
                {
                    case ConditionType.Set:
                        Condition = " SET ";
                        Condition += string.Join(" , ", dataClause);
                        break;
                    case ConditionType.Where:
                        Condition = " WHERE ";
                        Condition += string.Join(" AND ", dataClause);
                        break;
                }
            }
        }
    }
}
