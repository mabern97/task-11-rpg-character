﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace RPG_Character
{
    // CRUD Operations
    public class SQLHelper
    {
        /*
         * The connection string. Always private since this doesn't need
         * to be exposed anywhere. #staySafe
         */
        private string connectionString;

        public SQLHelper(string connectionString)
        {
            this.connectionString = connectionString;
        }

        // Create Operation (Insert)
        public void Create(string table, params object[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                InsertStatement insertStatement = new InsertStatement(table, parameters);
                string query = insertStatement.Statement;

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    // Add each value
                    for (int i = 0; i < insertStatement.Count; i++)
                    {
                        string id = insertStatement.identifiers[i];
                        object value = insertStatement.values[i];

                        cmd.Parameters.AddWithValue(id, value);
                    }
                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        // Read Operation (Select)
        public SelectResult Read(string table, SelectColumns columns, Conditions where)
        {
            SelectResult result = new SelectResult();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SelectStatement selectStatement = new SelectStatement(table, columns, where);
                string query = selectStatement.Statement;

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (selectStatement.Values.Count > 0)
                    {
                        foreach(KeyValuePair<string, object> value in selectStatement.Values)
                        {
                            cmd.Parameters.AddWithValue(value.Key, value.Value);
                        }
                    }

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        int columnCount = reader.FieldCount;

                        for (int i = 0; i < columnCount; i++)
                        {
                            string column = reader.GetName(i);
                            Console.WriteLine($"Column '{column}' is of type '{reader.GetDataTypeName(i)}'");
                            result.AddColumn(column);
                        }

                        int row = 0;
                        while (reader.Read())
                        {
                            object[] rowData = new object[columnCount];
                            for (int i = 0; i < columnCount; i++)
                            {
                                rowData[i] = reader.GetValue(i);

                                Console.WriteLine($"Value of {reader.GetName(i)} is {rowData[i]}");
                            }

                            result.AddValue(row, rowData);
                            row++;
                        }
                    }
                }

                conn.Close();
            }

            return result;
        }

        // Update Operation (UPDATE)
        public void Update(string table, Conditions set, Conditions where)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                UpdateStatement update = new UpdateStatement(table, set, where);
                string query = update.Statement;

                Console.WriteLine(query);

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (set.Values.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> value in set.Values)
                        {
                            cmd.Parameters.AddWithValue(value.Key, value.Value);
                        }
                    }

                    if (where.Values.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> value in where.Values)
                        {
                            cmd.Parameters.AddWithValue(value.Key, value.Value);
                        }
                    }

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        // Delete Operation (DELETE)
        public void Delete(string table, Conditions where)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                DeleteStatement delete = new DeleteStatement(table, where);
                string query = delete.Statement;

                Console.WriteLine(query);

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (where.Values.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> value in where.Values)
                        {
                            cmd.Parameters.AddWithValue(value.Key, value.Value);
                        }
                    }

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        // Parameter Count Logic
        public static bool IsParameterCountValid(object[] parameters)
        {
            return parameters.Length % 2 == 0;
        }
    }
}
