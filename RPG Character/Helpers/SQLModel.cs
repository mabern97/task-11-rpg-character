﻿using System;
using System.Collections.Generic;

namespace RPG_Character
{
    // Interface for the CRUD managers
    public interface SQLModel<T>
    {
        void Insert(T obj);         // Create

        T Get(Conditions where);    // Read
        ICollection<T> GetAll();    // Read
        void Delete(T obj);         // Update
        void Update(T obj);         // Delete
    }
}
