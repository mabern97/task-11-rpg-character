﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    // Generic abstract class for all CRUD managers
    public abstract class CRUDManager<T> : SQLModel<T>
    {
        // SQL helper that provides connection to the database and executes queries
        public SQLHelper helper;

        public CRUDManager(SQLHelper helper)
        {
            this.helper = helper;
        }

        // Insert method
        public abstract void Insert(T obj);

        // Get WHERE method
        public abstract T Get(Conditions where);

        // Get * method
        public abstract ICollection<T> GetAll();

        // Detete method
        public abstract void Delete(T obj);

        // Update method
        public abstract void Update(T obj);
    }
}
