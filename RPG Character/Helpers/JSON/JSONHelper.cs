﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Newtonsoft.Json;

namespace RPG_Character
{
    public class JSONHelper
    {
        // Give the opportunity to export one of more objects into the same file
        public static void ExportObject<T>(string filename, List<T> objectToExport)
        {
            // Create a new Stream that writes to a file
            using (StreamWriter writer = new StreamWriter(filename))
            {
                // Serialize the objects as a JSON String
                JsonSerializer json = new JsonSerializer();
                json.Serialize(writer, objectToExport);
            }
        }
    }
}
