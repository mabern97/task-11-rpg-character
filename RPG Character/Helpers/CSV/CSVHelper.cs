﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.IO;

using CsvHelper;
using System.Globalization;

namespace RPG_Character.Helpers.CSV
{
    public class CSVHelper
    {
        // Give the opportunity to export one of more objects into the same file
        public static void ExportObject<T>(string filename, List<T> objectToExport)
        {
            // Create a new Stream that writes to a file
            using (StreamWriter writer = new StreamWriter(filename))
            {
                using (CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    // Write a header (contains property names)
                    csv.WriteHeader(objectToExport[0].GetType());
                    csv.NextRecord();

                    // Write each property of each record into the CSV file
                    csv.WriteRecords(objectToExport);
                }
            }
        }
    }
}
