﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    public abstract class DBRow
    {
        public int Id { get; set; } // ID of the row
        public object Object { get; private set; } // Object that the model uses

        public DBRow(int id, object obj)
        {
            Id = id;
            Object = obj;
        }

        public string GetRowType()
        {
            return GetType().Name;
        }
    }
}
