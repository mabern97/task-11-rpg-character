﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RPG_Character
{
    /*
     * Abstact class: Entity
     * 
     * The entity abstract class is what all entities inherit.
     * All entities have a name
     */
    public abstract class Entity
    {
        public string Name { get; set; }

        public string Type
        {
            get { return GetType().Name; }
        }

        public static Type[] EntityTypes
        {
            get
            {
                return new Type[]
                {
                    typeof(Character),
                    typeof(Weapon),
                };
            }
        }

        public Entity(string name)
        {
            Name = name;
        }
    }
}
