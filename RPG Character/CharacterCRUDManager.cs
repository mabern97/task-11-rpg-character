﻿using CsvHelper;
using System;
using System.Collections.Generic;

namespace RPG_Character
{
    public class CharacterRow : DBRow
    {
        public CharacterRow(int id, Character character)
            :base(id, character)
        {
        }
    }

    public class CharacterCRUDManager : CRUDManager<CharacterRow>
    {
        public CharacterCRUDManager(SQLHelper helper)
            :base(helper)
        {

        }

        public override void Insert(CharacterRow objRow)
        {
            Character obj = (Character)objRow.Object;
            string charType = obj.GetCharacterType();

            helper.Create("Characters", "Name", obj.Name,
                                        "Type", obj.GetCharacterType(),
                                        "Health", obj.Health,
                                        "Armor", obj.Armor,
                                        "Energy", obj.Energy,
                                        "Mana", obj.Mana);
        }

        public override CharacterRow Get(Conditions where)
        {
            CharacterRow row = null;

            SelectColumns columns = new SelectColumns();
            SelectResult result = helper.Read("Characters", columns, where);

            return row;
        }

        public override ICollection<CharacterRow> GetAll()
        {
            ICollection<CharacterRow> characters = new List<CharacterRow>();

            SelectColumns columns = new SelectColumns();
            Conditions where = new Conditions(ConditionType.Where);
            SelectResult result = helper.Read("Characters", columns, where);

            for(int i = 0; i < result.RowCount; i++)
            {
                Dictionary<string, object> rowData = result.GetRowValues(i);

                foreach(KeyValuePair<string, object> item in rowData)
                {
                    Console.WriteLine($"{item.Key} => {item.Value}");
                }

                int id = (int)rowData["ID"];
                string name = (string)rowData["Name"];
                string type = (string)rowData["Type"];
                int health = (int)rowData["Health"];
                int armor = (int)rowData["Armor"];
                int energy = (int)rowData["Energy"];
                int mana = (int)rowData["Mana"];

                Character character = null;
                switch(type)
                {
                    case "Warrior":
                        character = new Warrior(name);
                        break;
                    case "Wizard":
                        character = new Wizard(name);
                        break;
                    case "Thief":
                        character = new Thief(name);
                        break;
                    case "Mage":
                        character = new Mage(name);
                        break;
                    case "Gangster":
                        character = new Gangster(name);
                        break;
                }
                character.Health = health;
                character.Armor = armor;
                character.Energy = energy;
                character.Mana = mana;

                characters.Add(new CharacterRow(id, character));
            }

            return characters;
        }

        public override void Update(CharacterRow objRow)
        {
            Character obj = (Character)objRow.Object;
            //throw new NotImplementedException();
            Conditions set = new Conditions(ConditionType.Set, "Name", obj.Name,
                                                               "Health", obj.Health,
                                                               "Armor", obj.Armor,
                                                               "Energy", obj.Energy,
                                                               "Mana", obj.Mana);

            Conditions where = new Conditions(ConditionType.Where, "ID", objRow.Id);

            helper.Update("Characters", set, where);
        }

        public override void Delete(CharacterRow objRow)
        {
            Conditions where = new Conditions(ConditionType.Where, "ID", objRow.Id);
            helper.Delete("Characters", where);
        }
    }
}
