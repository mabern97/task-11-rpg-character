﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Character
{
    public class WeaponRow : DBRow
    {

        public WeaponRow(int id, Weapon weapon)
            :base(id, weapon)
        {
        }
    }
    public class WeaponCRUDManager : CRUDManager<WeaponRow>
    {
        public WeaponCRUDManager(SQLHelper helper)
            :base(helper)
        {
            
        }

        public override void Insert(WeaponRow objRow)
        {
            Weapon obj = (Weapon)objRow.Object;

            helper.Create("Weapons", "Type", obj.GetWeaponType(),
                                     "Name", obj.Name,
                                     "Nickname", obj.Nickname,
                                     "FireRate", obj.FireRate,
                                     "DamagePerHit", obj.DamagePerHit,
                                     "Range", obj.Range,
                                     "ClipSize", obj.ClipSize,
                                     "MaxAmmo", obj.MaxAmmo);
        }

        public override WeaponRow Get(Conditions where)
        {
            throw new NotImplementedException();
        }

        public override ICollection<WeaponRow> GetAll()
        {
            ICollection<WeaponRow> weapons = new List<WeaponRow>();

            SelectColumns columns = new SelectColumns();
            Conditions where = new Conditions(ConditionType.Where);
            SelectResult result = helper.Read("Weapons", columns, where);

            for (int i = 0; i < result.RowCount; i++)
            {
                Dictionary<string, object> rowData = result.GetRowValues(i);

                foreach (KeyValuePair<string, object> item in rowData)
                {
                    Console.WriteLine($"{item.Key} => {item.Value}");
                }

                int id = (int)rowData["ID"];
                string type = (string)rowData["Type"];
                string name = (string)rowData["Name"];
                string nickname = (string)rowData["Nickname"];
                int fireRate = (int)rowData["FireRate"];
                int dmgPerHit = (int)rowData["DamagePerHit"];
                int range = (int)rowData["Range"];
                int clipSize = (int)rowData["ClipSize"];
                int maxAmmo = (int)rowData["MaxAmmo"];


                Weapon weapon = null;
                switch (type)
                {
                    case "Pistol":
                        weapon = new Pistol(name, nickname);
                        break;
                    case "Shotgun":
                        weapon = new Shotgun(name, nickname);
                        break;
                    case "SemiAutomatic":
                        weapon = new SemiAutomatic(name, nickname);
                        break;
                    case "Rifle":
                        weapon = new Rifle(name, nickname);
                        break;
                    case "Mystical":
                        weapon = new Mystical(name, nickname);
                        break;
                }
                weapon.FireRate = fireRate;
                weapon.DamagePerHit = dmgPerHit;
                weapon.Range = range;
                weapon.ClipSize = clipSize;
                weapon.MaxAmmo = maxAmmo;

                weapons.Add(new WeaponRow(id, weapon));
            }

            return weapons;
        }

        public override void Update(WeaponRow objRow)
        {
            Weapon obj = (Weapon)objRow.Object;
            Conditions set = new Conditions(ConditionType.Set, "Name", obj.Name,
                                                               "Nickname", obj.Nickname,
                                                               "FireRate", obj.FireRate,
                                                               "DamagePerHit", obj.DamagePerHit,
                                                               "Range", obj.Range,
                                                               "ClipSize", obj.ClipSize,
                                                               "MaxAmmo", obj.MaxAmmo);

            Conditions where = new Conditions(ConditionType.Where, "ID", objRow.Id);

            helper.Update("Weapons", set, where);
        }

        public override void Delete(WeaponRow objRow)
        {
            Conditions where = new Conditions(ConditionType.Where, "ID", objRow.Id);
            helper.Delete("Weapons", where);
        }
    }
}
