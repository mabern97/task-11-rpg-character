﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG_Character;

namespace RPG_Character_UI
{
    public class WeaponHandler
    {
        private WeaponCRUDManager crud;
        private MainForm form;

        private List<WeaponRow> dbRows; // Database Entries for weapons

        public WeaponHandler(MainForm mainForm, SQLHelper helper)
        {
            dbRows = new List<WeaponRow>();
            form = mainForm;
            crud = new WeaponCRUDManager(helper);

            ICollection<WeaponRow> results = crud.GetAll();

            foreach (WeaponRow row in results)
                dbRows.Add(row);
        }

        public void CreateWeapon(WeaponRow row)
        {
            crud.Insert(row);
        }

        public List<WeaponRow> GetWeapons()
        {
            ICollection<WeaponRow> result = crud.GetAll();

            dbRows.Clear();
            foreach (WeaponRow row in result)
                dbRows.Add(row);

            return dbRows;
        }

        public void UpdateWeapon(WeaponRow row)
        {
            crud.Update(row);
        }

        public void DeleteWeapon(WeaponRow row)
        {
            crud.Delete(row);
        }
    }
}
