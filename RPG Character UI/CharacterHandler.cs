﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPG_Character;

namespace RPG_Character_UI
{
    public class CharacterHandler
    {
        private CharacterCRUDManager crud;
        private MainForm form;

        private List<CharacterRow> dbRows; // Database Entries for characters

        public CharacterHandler(MainForm mainForm, SQLHelper helper)
        {
            dbRows = new List<CharacterRow>();
            form = mainForm;
            crud = new CharacterCRUDManager(helper);

            ICollection<CharacterRow> results = crud.GetAll();

            foreach(CharacterRow row in results)
                dbRows.Add(row);
        }

        public void CreateCharacter(CharacterRow row)
        {
            crud.Insert(row);
        }

        public List<CharacterRow> GetCharacters()
        {
            ICollection<CharacterRow> result = crud.GetAll();

            dbRows.Clear();
            foreach (CharacterRow row in result)
                dbRows.Add(row);

            return dbRows;
        }

        public void UpdateCharacter(CharacterRow row)
        {
            crud.Update(row);
        }

        public void DeleteCharacter(CharacterRow row)
        {
            crud.Delete(row);
        }
    }
}
