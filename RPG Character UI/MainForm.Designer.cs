﻿namespace RPG_Character_UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAsCSVToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAsJSONToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.createEntityButton = new System.Windows.Forms.ToolStripButton();
            this.deleteButton = new System.Windows.Forms.ToolStripButton();
            this.updateButton = new System.Windows.Forms.ToolStripButton();
            this.duplicateButton = new System.Windows.Forms.ToolStripButton();
            this.entityViewer = new System.Windows.Forms.TreeView();
            this.refreshButton = new System.Windows.Forms.Button();
            this.charGroup = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.charTypeBox = new System.Windows.Forms.ComboBox();
            this.charTypeLbl = new System.Windows.Forms.Label();
            this.charManaBox = new System.Windows.Forms.NumericUpDown();
            this.charEnergyBox = new System.Windows.Forms.NumericUpDown();
            this.charArmorBox = new System.Windows.Forms.NumericUpDown();
            this.charHPBox = new System.Windows.Forms.NumericUpDown();
            this.charNameBox = new System.Windows.Forms.TextBox();
            this.charManaLbl = new System.Windows.Forms.Label();
            this.charEnergyLbl = new System.Windows.Forms.Label();
            this.charArmorLbl = new System.Windows.Forms.Label();
            this.charHPLbl = new System.Windows.Forms.Label();
            this.charNameLbl = new System.Windows.Forms.Label();
            this.weapGroup = new System.Windows.Forms.GroupBox();
            this.weapTypeBox = new System.Windows.Forms.ComboBox();
            this.weapTypeLbl = new System.Windows.Forms.Label();
            this.weapDmgPerHitLbl = new System.Windows.Forms.Label();
            this.weapDmgPerHitBox = new System.Windows.Forms.NumericUpDown();
            this.weapNickBox = new System.Windows.Forms.TextBox();
            this.weapMaxAmmoBox = new System.Windows.Forms.NumericUpDown();
            this.weapClipBox = new System.Windows.Forms.NumericUpDown();
            this.weapRangeBox = new System.Windows.Forms.NumericUpDown();
            this.weapRateBox = new System.Windows.Forms.NumericUpDown();
            this.weapNameBox = new System.Windows.Forms.TextBox();
            this.weapMaxAmmoLbl = new System.Windows.Forms.Label();
            this.weapClipSizeLbl = new System.Windows.Forms.Label();
            this.weapRangeLbl = new System.Windows.Forms.Label();
            this.weapFirerateLbl = new System.Windows.Forms.Label();
            this.weapNickLbl = new System.Windows.Forms.Label();
            this.weapNameLbl = new System.Windows.Forms.Label();
            this.deselectButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.logText = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.charGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.charManaBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.charEnergyBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.charArmorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.charHPBox)).BeginInit();
            this.weapGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weapDmgPerHitBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weapMaxAmmoBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weapClipBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weapRangeBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weapRateBox)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(504, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Existing Entities";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.exportToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(3, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(749, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 22);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeyDisplayString = "Alt-F4";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // exportToolStripMenuItem1
            // 
            this.exportToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportAsCSVToolStripMenuItem1,
            this.exportAsJSONToolStripMenuItem1});
            this.exportToolStripMenuItem1.Name = "exportToolStripMenuItem1";
            this.exportToolStripMenuItem1.Size = new System.Drawing.Size(53, 22);
            this.exportToolStripMenuItem1.Text = "Export";
            // 
            // exportAsCSVToolStripMenuItem1
            // 
            this.exportAsCSVToolStripMenuItem1.Name = "exportAsCSVToolStripMenuItem1";
            this.exportAsCSVToolStripMenuItem1.Size = new System.Drawing.Size(155, 22);
            this.exportAsCSVToolStripMenuItem1.Text = "Export As CSV";
            // 
            // exportAsJSONToolStripMenuItem1
            // 
            this.exportAsJSONToolStripMenuItem1.Name = "exportAsJSONToolStripMenuItem1";
            this.exportAsJSONToolStripMenuItem1.Size = new System.Drawing.Size(155, 22);
            this.exportAsJSONToolStripMenuItem1.Text = "Export As JSON";
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createEntityButton,
            this.deleteButton,
            this.updateButton,
            this.duplicateButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(749, 25);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // createEntityButton
            // 
            this.createEntityButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.createEntityButton.Image = ((System.Drawing.Image)(resources.GetObject("createEntityButton.Image")));
            this.createEntityButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.createEntityButton.Name = "createEntityButton";
            this.createEntityButton.Size = new System.Drawing.Size(78, 22);
            this.createEntityButton.Text = "Create Entity";
            // 
            // deleteButton
            // 
            this.deleteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.deleteButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteButton.Image")));
            this.deleteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(77, 22);
            this.deleteButton.Text = "Delete Entity";
            // 
            // updateButton
            // 
            this.updateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.updateButton.Image = ((System.Drawing.Image)(resources.GetObject("updateButton.Image")));
            this.updateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(82, 22);
            this.updateButton.Text = "Update Entity";
            // 
            // duplicateButton
            // 
            this.duplicateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.duplicateButton.Image = ((System.Drawing.Image)(resources.GetObject("duplicateButton.Image")));
            this.duplicateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.duplicateButton.Name = "duplicateButton";
            this.duplicateButton.Size = new System.Drawing.Size(94, 22);
            this.duplicateButton.Text = "Duplicate Entity";
            // 
            // entityViewer
            // 
            this.entityViewer.HideSelection = false;
            this.entityViewer.Location = new System.Drawing.Point(504, 63);
            this.entityViewer.Margin = new System.Windows.Forms.Padding(1);
            this.entityViewer.Name = "entityViewer";
            this.entityViewer.ShowPlusMinus = false;
            this.entityViewer.Size = new System.Drawing.Size(241, 383);
            this.entityViewer.TabIndex = 7;
            // 
            // refreshButton
            // 
            this.refreshButton.Location = new System.Drawing.Point(504, 447);
            this.refreshButton.Margin = new System.Windows.Forms.Padding(0);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(241, 30);
            this.refreshButton.TabIndex = 8;
            this.refreshButton.Text = "Refresh List";
            this.refreshButton.UseVisualStyleBackColor = true;
            // 
            // charGroup
            // 
            this.charGroup.Controls.Add(this.label2);
            this.charGroup.Controls.Add(this.charTypeBox);
            this.charGroup.Controls.Add(this.charTypeLbl);
            this.charGroup.Controls.Add(this.charManaBox);
            this.charGroup.Controls.Add(this.charEnergyBox);
            this.charGroup.Controls.Add(this.charArmorBox);
            this.charGroup.Controls.Add(this.charHPBox);
            this.charGroup.Controls.Add(this.charNameBox);
            this.charGroup.Controls.Add(this.charManaLbl);
            this.charGroup.Controls.Add(this.charEnergyLbl);
            this.charGroup.Controls.Add(this.charArmorLbl);
            this.charGroup.Controls.Add(this.charHPLbl);
            this.charGroup.Controls.Add(this.charNameLbl);
            this.charGroup.Location = new System.Drawing.Point(12, 63);
            this.charGroup.Name = "charGroup";
            this.charGroup.Size = new System.Drawing.Size(487, 196);
            this.charGroup.TabIndex = 9;
            this.charGroup.TabStop = false;
            this.charGroup.Text = "Character Attributes";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(340, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 23);
            this.label2.TabIndex = 12;
            this.label2.Text = "Available Actions";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // charTypeBox
            // 
            this.charTypeBox.Enabled = false;
            this.charTypeBox.FormattingEnabled = true;
            this.charTypeBox.Location = new System.Drawing.Point(112, 45);
            this.charTypeBox.Name = "charTypeBox";
            this.charTypeBox.Size = new System.Drawing.Size(195, 21);
            this.charTypeBox.TabIndex = 11;
            // 
            // charTypeLbl
            // 
            this.charTypeLbl.Location = new System.Drawing.Point(6, 43);
            this.charTypeLbl.Name = "charTypeLbl";
            this.charTypeLbl.Size = new System.Drawing.Size(100, 23);
            this.charTypeLbl.TabIndex = 10;
            this.charTypeLbl.Text = "Character Type:";
            this.charTypeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // charManaBox
            // 
            this.charManaBox.Location = new System.Drawing.Point(112, 150);
            this.charManaBox.Name = "charManaBox";
            this.charManaBox.Size = new System.Drawing.Size(195, 20);
            this.charManaBox.TabIndex = 9;
            // 
            // charEnergyBox
            // 
            this.charEnergyBox.Location = new System.Drawing.Point(112, 124);
            this.charEnergyBox.Name = "charEnergyBox";
            this.charEnergyBox.Size = new System.Drawing.Size(195, 20);
            this.charEnergyBox.TabIndex = 8;
            // 
            // charArmorBox
            // 
            this.charArmorBox.Location = new System.Drawing.Point(112, 98);
            this.charArmorBox.Name = "charArmorBox";
            this.charArmorBox.Size = new System.Drawing.Size(195, 20);
            this.charArmorBox.TabIndex = 7;
            // 
            // charHPBox
            // 
            this.charHPBox.Location = new System.Drawing.Point(112, 72);
            this.charHPBox.Name = "charHPBox";
            this.charHPBox.Size = new System.Drawing.Size(195, 20);
            this.charHPBox.TabIndex = 6;
            // 
            // charNameBox
            // 
            this.charNameBox.Location = new System.Drawing.Point(112, 19);
            this.charNameBox.MaxLength = 50;
            this.charNameBox.Name = "charNameBox";
            this.charNameBox.Size = new System.Drawing.Size(195, 20);
            this.charNameBox.TabIndex = 5;
            // 
            // charManaLbl
            // 
            this.charManaLbl.Location = new System.Drawing.Point(6, 147);
            this.charManaLbl.Name = "charManaLbl";
            this.charManaLbl.Size = new System.Drawing.Size(100, 23);
            this.charManaLbl.TabIndex = 4;
            this.charManaLbl.Text = "Mana:";
            this.charManaLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // charEnergyLbl
            // 
            this.charEnergyLbl.Location = new System.Drawing.Point(6, 121);
            this.charEnergyLbl.Name = "charEnergyLbl";
            this.charEnergyLbl.Size = new System.Drawing.Size(100, 23);
            this.charEnergyLbl.TabIndex = 3;
            this.charEnergyLbl.Text = "Energy:";
            this.charEnergyLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // charArmorLbl
            // 
            this.charArmorLbl.Location = new System.Drawing.Point(6, 95);
            this.charArmorLbl.Name = "charArmorLbl";
            this.charArmorLbl.Size = new System.Drawing.Size(100, 23);
            this.charArmorLbl.TabIndex = 2;
            this.charArmorLbl.Text = "Armor:";
            this.charArmorLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // charHPLbl
            // 
            this.charHPLbl.Location = new System.Drawing.Point(6, 69);
            this.charHPLbl.Name = "charHPLbl";
            this.charHPLbl.Size = new System.Drawing.Size(100, 23);
            this.charHPLbl.TabIndex = 1;
            this.charHPLbl.Text = "Health:";
            this.charHPLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // charNameLbl
            // 
            this.charNameLbl.Location = new System.Drawing.Point(6, 17);
            this.charNameLbl.Name = "charNameLbl";
            this.charNameLbl.Size = new System.Drawing.Size(100, 23);
            this.charNameLbl.TabIndex = 0;
            this.charNameLbl.Text = "Character Name:";
            this.charNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weapGroup
            // 
            this.weapGroup.Controls.Add(this.weapTypeBox);
            this.weapGroup.Controls.Add(this.weapTypeLbl);
            this.weapGroup.Controls.Add(this.weapDmgPerHitLbl);
            this.weapGroup.Controls.Add(this.weapDmgPerHitBox);
            this.weapGroup.Controls.Add(this.weapNickBox);
            this.weapGroup.Controls.Add(this.weapMaxAmmoBox);
            this.weapGroup.Controls.Add(this.weapClipBox);
            this.weapGroup.Controls.Add(this.weapRangeBox);
            this.weapGroup.Controls.Add(this.weapRateBox);
            this.weapGroup.Controls.Add(this.weapNameBox);
            this.weapGroup.Controls.Add(this.weapMaxAmmoLbl);
            this.weapGroup.Controls.Add(this.weapClipSizeLbl);
            this.weapGroup.Controls.Add(this.weapRangeLbl);
            this.weapGroup.Controls.Add(this.weapFirerateLbl);
            this.weapGroup.Controls.Add(this.weapNickLbl);
            this.weapGroup.Controls.Add(this.weapNameLbl);
            this.weapGroup.Location = new System.Drawing.Point(12, 265);
            this.weapGroup.Name = "weapGroup";
            this.weapGroup.Size = new System.Drawing.Size(487, 246);
            this.weapGroup.TabIndex = 10;
            this.weapGroup.TabStop = false;
            this.weapGroup.Text = "Weapon Attributes";
            // 
            // weapTypeBox
            // 
            this.weapTypeBox.Enabled = false;
            this.weapTypeBox.FormattingEnabled = true;
            this.weapTypeBox.Location = new System.Drawing.Point(112, 45);
            this.weapTypeBox.Name = "weapTypeBox";
            this.weapTypeBox.Size = new System.Drawing.Size(195, 21);
            this.weapTypeBox.TabIndex = 17;
            // 
            // weapTypeLbl
            // 
            this.weapTypeLbl.Location = new System.Drawing.Point(6, 45);
            this.weapTypeLbl.Name = "weapTypeLbl";
            this.weapTypeLbl.Size = new System.Drawing.Size(100, 23);
            this.weapTypeLbl.TabIndex = 16;
            this.weapTypeLbl.Text = "Weapon Type:";
            this.weapTypeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weapDmgPerHitLbl
            // 
            this.weapDmgPerHitLbl.Location = new System.Drawing.Point(6, 199);
            this.weapDmgPerHitLbl.Name = "weapDmgPerHitLbl";
            this.weapDmgPerHitLbl.Size = new System.Drawing.Size(100, 23);
            this.weapDmgPerHitLbl.TabIndex = 15;
            this.weapDmgPerHitLbl.Text = "Damage Per Hit:";
            this.weapDmgPerHitLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weapDmgPerHitBox
            // 
            this.weapDmgPerHitBox.Location = new System.Drawing.Point(112, 202);
            this.weapDmgPerHitBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.weapDmgPerHitBox.Name = "weapDmgPerHitBox";
            this.weapDmgPerHitBox.Size = new System.Drawing.Size(195, 20);
            this.weapDmgPerHitBox.TabIndex = 14;
            // 
            // weapNickBox
            // 
            this.weapNickBox.Location = new System.Drawing.Point(112, 72);
            this.weapNickBox.MaxLength = 50;
            this.weapNickBox.Name = "weapNickBox";
            this.weapNickBox.Size = new System.Drawing.Size(195, 20);
            this.weapNickBox.TabIndex = 13;
            // 
            // weapMaxAmmoBox
            // 
            this.weapMaxAmmoBox.Location = new System.Drawing.Point(112, 176);
            this.weapMaxAmmoBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.weapMaxAmmoBox.Name = "weapMaxAmmoBox";
            this.weapMaxAmmoBox.Size = new System.Drawing.Size(195, 20);
            this.weapMaxAmmoBox.TabIndex = 11;
            // 
            // weapClipBox
            // 
            this.weapClipBox.Location = new System.Drawing.Point(112, 150);
            this.weapClipBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.weapClipBox.Name = "weapClipBox";
            this.weapClipBox.Size = new System.Drawing.Size(195, 20);
            this.weapClipBox.TabIndex = 10;
            // 
            // weapRangeBox
            // 
            this.weapRangeBox.Location = new System.Drawing.Point(112, 124);
            this.weapRangeBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.weapRangeBox.Name = "weapRangeBox";
            this.weapRangeBox.Size = new System.Drawing.Size(195, 20);
            this.weapRangeBox.TabIndex = 9;
            // 
            // weapRateBox
            // 
            this.weapRateBox.Location = new System.Drawing.Point(112, 98);
            this.weapRateBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.weapRateBox.Name = "weapRateBox";
            this.weapRateBox.Size = new System.Drawing.Size(195, 20);
            this.weapRateBox.TabIndex = 8;
            // 
            // weapNameBox
            // 
            this.weapNameBox.Location = new System.Drawing.Point(112, 19);
            this.weapNameBox.MaxLength = 50;
            this.weapNameBox.Name = "weapNameBox";
            this.weapNameBox.Size = new System.Drawing.Size(195, 20);
            this.weapNameBox.TabIndex = 7;
            // 
            // weapMaxAmmoLbl
            // 
            this.weapMaxAmmoLbl.Location = new System.Drawing.Point(6, 173);
            this.weapMaxAmmoLbl.Name = "weapMaxAmmoLbl";
            this.weapMaxAmmoLbl.Size = new System.Drawing.Size(100, 23);
            this.weapMaxAmmoLbl.TabIndex = 5;
            this.weapMaxAmmoLbl.Text = "Maximum Ammo:";
            this.weapMaxAmmoLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weapClipSizeLbl
            // 
            this.weapClipSizeLbl.Location = new System.Drawing.Point(6, 147);
            this.weapClipSizeLbl.Name = "weapClipSizeLbl";
            this.weapClipSizeLbl.Size = new System.Drawing.Size(100, 23);
            this.weapClipSizeLbl.TabIndex = 4;
            this.weapClipSizeLbl.Text = "Clip Size:";
            this.weapClipSizeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weapRangeLbl
            // 
            this.weapRangeLbl.Location = new System.Drawing.Point(6, 121);
            this.weapRangeLbl.Name = "weapRangeLbl";
            this.weapRangeLbl.Size = new System.Drawing.Size(100, 23);
            this.weapRangeLbl.TabIndex = 3;
            this.weapRangeLbl.Text = "Range:";
            this.weapRangeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weapFirerateLbl
            // 
            this.weapFirerateLbl.Location = new System.Drawing.Point(6, 95);
            this.weapFirerateLbl.Name = "weapFirerateLbl";
            this.weapFirerateLbl.Size = new System.Drawing.Size(100, 23);
            this.weapFirerateLbl.TabIndex = 2;
            this.weapFirerateLbl.Text = "Fire Rate:";
            this.weapFirerateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weapNickLbl
            // 
            this.weapNickLbl.Location = new System.Drawing.Point(6, 70);
            this.weapNickLbl.Name = "weapNickLbl";
            this.weapNickLbl.Size = new System.Drawing.Size(100, 23);
            this.weapNickLbl.TabIndex = 1;
            this.weapNickLbl.Text = "Nickname:";
            this.weapNickLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weapNameLbl
            // 
            this.weapNameLbl.Location = new System.Drawing.Point(6, 17);
            this.weapNameLbl.Name = "weapNameLbl";
            this.weapNameLbl.Size = new System.Drawing.Size(100, 23);
            this.weapNameLbl.TabIndex = 0;
            this.weapNameLbl.Text = "Weapon Name:";
            this.weapNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // deselectButton
            // 
            this.deselectButton.Location = new System.Drawing.Point(504, 481);
            this.deselectButton.Margin = new System.Windows.Forms.Padding(0);
            this.deselectButton.Name = "deselectButton";
            this.deselectButton.Size = new System.Drawing.Size(241, 30);
            this.deselectButton.TabIndex = 11;
            this.deselectButton.Text = "Deselect Entity";
            this.deselectButton.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.AllowMerge = false;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 663);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(749, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.logText);
            this.groupBox1.Location = new System.Drawing.Point(12, 517);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(733, 143);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Application Log";
            // 
            // logText
            // 
            this.logText.Location = new System.Drawing.Point(9, 19);
            this.logText.Multiline = true;
            this.logText.Name = "logText";
            this.logText.ReadOnly = true;
            this.logText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logText.Size = new System.Drawing.Size(716, 118);
            this.logText.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 685);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.deselectButton);
            this.Controls.Add(this.weapGroup);
            this.Controls.Add(this.charGroup);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.entityViewer);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RPG Entity Manager";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.charGroup.ResumeLayout(false);
            this.charGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.charManaBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.charEnergyBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.charArmorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.charHPBox)).EndInit();
            this.weapGroup.ResumeLayout(false);
            this.weapGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weapDmgPerHitBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weapMaxAmmoBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weapClipBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weapRangeBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weapRateBox)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        public System.Windows.Forms.TreeView entityViewer;
        public System.Windows.Forms.ToolStripButton deleteButton;
        public System.Windows.Forms.ToolStripButton updateButton;
        public System.Windows.Forms.Button refreshButton;

        public System.Windows.Forms.GroupBox charGroup;
        private System.Windows.Forms.Label charArmorLbl;
        private System.Windows.Forms.Label charHPLbl;
        private System.Windows.Forms.Label charNameLbl;
        private System.Windows.Forms.Label charEnergyLbl;
        private System.Windows.Forms.Label charManaLbl;
        public System.Windows.Forms.TextBox charNameBox;
        public System.Windows.Forms.NumericUpDown charManaBox;
        public System.Windows.Forms.NumericUpDown charEnergyBox;
        public System.Windows.Forms.NumericUpDown charArmorBox;
        public System.Windows.Forms.NumericUpDown charHPBox;

        public System.Windows.Forms.GroupBox weapGroup;
        private System.Windows.Forms.Label weapNameLbl;
        private System.Windows.Forms.Label weapNickLbl;
        private System.Windows.Forms.Label weapFirerateLbl;
        private System.Windows.Forms.Label weapRangeLbl;
        private System.Windows.Forms.Label weapClipSizeLbl;
        private System.Windows.Forms.Label weapMaxAmmoLbl;
        public System.Windows.Forms.TextBox weapNameBox;
        public System.Windows.Forms.TextBox weapNickBox;
        public System.Windows.Forms.NumericUpDown weapMaxAmmoBox;
        public System.Windows.Forms.NumericUpDown weapClipBox;
        public System.Windows.Forms.NumericUpDown weapRangeBox;
        public System.Windows.Forms.NumericUpDown weapRateBox;

        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportAsCSVToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportAsJSONToolStripMenuItem1;
        private System.Windows.Forms.Label weapDmgPerHitLbl;
        public System.Windows.Forms.NumericUpDown weapDmgPerHitBox;
        public System.Windows.Forms.Button deselectButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel statusLabel;
        public System.Windows.Forms.ComboBox charTypeBox;
        private System.Windows.Forms.Label charTypeLbl;
        public System.Windows.Forms.ComboBox weapTypeBox;
        private System.Windows.Forms.Label weapTypeLbl;
        public System.Windows.Forms.ToolStripButton duplicateButton;
        public System.Windows.Forms.ToolStripButton createEntityButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox logText;
    }
}

