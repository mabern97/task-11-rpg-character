﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using RPG_Character;

namespace RPG_Character_UI
{
    public partial class CreationForm : Form
    {
        public event EventHandler onEntityCreate;

        protected virtual void onEntityCreated(EventArgs e)
        {
            EventHandler handler = onEntityCreate;
            handler?.Invoke(this, e);
        }

        public CreationForm()
        {
            InitializeComponent();

            entityCategory.SelectedIndexChanged += onCategorySelected;

            createButton.Click += onCreateClick;
            cancelButton.Click += onCancelClick;
        }

        private void onCancelClick(object sender, EventArgs e)
        {
            Close();
        }

        private void onCreateClick(object sender, EventArgs e)
        {
            int categoryIndex = entityCategory.SelectedIndex;
            int typeIndex = entityTypeBox.SelectedIndex;
            string entityName = entityNameBox.Text;

            Type entityObjectType = null;
            switch(categoryIndex)
            {
                case 0: // Character
                    entityObjectType = Character.Characters[typeIndex];
                    break;
                case 1: // Weapon
                    entityObjectType = Weapon.DefinedWeapons[typeIndex];
                    break;
            }

            if (entityName.Equals(string.Empty))
            {
                MessageBox.Show("Please enter a name for your new entity.");
                return;
            }

            object[] arguments = new object[2];
            arguments[0] = entityObjectType;
            arguments[1] = entityName;

            onEntityCreate(arguments, null);
            Close();
        }

        private void onCategorySelected(object sender, EventArgs e)
        {
            int index = (sender as ComboBox).SelectedIndex;
            Type type = Entity.EntityTypes[index];

            RetrieveEntityTypesFromCategory(type);
        }

        private void RetrieveEntityTypesFromCategory(Type type)
        {
            entityTypeBox.Items.Clear();
            switch (type.Name)
            {
                case "Character":
                    foreach(Type charType in Character.Characters)
                    {
                        entityTypeBox.Items.Add(charType.Name);
                    }
                    break;
                case "Weapon":
                    foreach (Type weapType in Weapon.DefinedWeapons)
                    {
                        entityTypeBox.Items.Add(weapType.Name);
                    }
                    break;
            }
            entityTypeBox.SelectedIndex = 0;
        }

        private void Reset()
        {
            entityCategory.ResetText();
            entityTypeBox.ResetText();
            entityNameBox.ResetText();

            entityCategory.Items.Clear();
            entityTypeBox.Items.Clear();
        }

        public void Show()
        {
            Reset();

            Type[] entityTypes = Entity.EntityTypes;

            foreach(Type entityType in entityTypes)
            {
                entityCategory.Items.Add(entityType.Name);
            }

            entityCategory.SelectedIndex = 0;

            ShowDialog();
        }
    }
}
