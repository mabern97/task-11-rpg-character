﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using RPG_Character;

namespace RPG_Character_UI
{
    public class UI_Tree
    {
        private CharacterHandler charHandler;
        private WeaponHandler weapHandler;

        private TreeView treeView;
        private Button refreshButton;
        private Button deselectButton;
        private ToolStripStatusLabel statusLabel;

        // Fonts
        private Font fontBold;
        private Font fontItalic;
        private Font fontRegular;

        // Node to Entity Dictionary
        Dictionary<TreeNode, DBRow> entities;

        // Unclickable tree elements
        List<TreeNode> unclickable;

        // Events
        public event EventHandler onEntityClick;
        public event EventHandler onEntityDeselect;

        protected virtual void OnEntityClicked(EventArgs e)
        {
            EventHandler handler = onEntityClick;
            handler?.Invoke(this, e);
        }

        protected virtual void OnEntityDeselected(EventArgs e)
        {
            EventHandler handler = onEntityDeselect;
            handler?.Invoke(this, e);
        }

        public UI_Tree(MainForm form, CharacterHandler charHandler, WeaponHandler weapHandler)
        {
            treeView = form.entityViewer;
            deselectButton = form.deselectButton;
            refreshButton = form.refreshButton;
            statusLabel = form.statusLabel;

            entities = new Dictionary<TreeNode, DBRow>();
            unclickable = new List<TreeNode>();

            // Font setup
            fontBold = new Font(TreeView.DefaultFont, FontStyle.Bold);
            fontItalic = new Font(TreeView.DefaultFont, FontStyle.Italic);
            fontRegular = new Font(TreeView.DefaultFont, FontStyle.Regular);

            // Assign variables
            this.treeView = form.entityViewer;
            this.charHandler = charHandler;
            this.weapHandler = weapHandler;

            treeView.Font = fontBold;

            // Event Handlers
            treeView.NodeMouseClick += (sender, e) => {
                if (unclickable.Contains(e.Node))
                {
                    treeView.SelectedNode = null;

                    return;
                }

                Select(e.Node);
            };

            deselectButton.Click += (sender, e) =>
            {
                onEntityDeselect(null, null);
                Deselect();
                statusLabel.Text = "Deselected entity";
            };

            refreshButton.Click += (sender, e) =>
            {
                PopulateEntityTree();
                onEntityDeselect(null, null);
                statusLabel.Text = "Refreshed entity list";
            };
        }

        public void Select(TreeNode node)
        {
            if (node == null)
                return;

            DBRow row = entities[node];
            onEntityClick(row, null);
            treeView.SelectedNode = node;
            deselectButton.Enabled = true;
        }

        public void Deselect()
        {
            treeView.SelectedNode = null;
            deselectButton.Enabled = false;
        }

        public TreeNode FindNode(string Name)
        {
            TreeNode node = null;

            List<TreeNode> nodes = new List<TreeNode>();
            foreach(TreeNode n in treeView.Nodes)
            {
                nodes.Add(n);

                // Nodes inside Category
                foreach(TreeNode n1 in n.Nodes)
                {
                    // Nodes inside Type
                    foreach (TreeNode n2 in n1.Nodes)
                        nodes.Add(n2);

                    nodes.Add(n1);
                }
            }

            foreach (TreeNode n in nodes)
                if (n.Text == Name)
                {
                    node = n;
                    break;
                }

            return node;
        }

        public void PopulateEntityTree()
        {
            Deselect(); // Deselect just incase
            treeView.Nodes.Clear(); // Clear the tree

            // Add Entity Categories
            TreeNode charCat = treeView.Nodes.Add("Characters");
            TreeNode weaponCat = treeView.Nodes.Add("Weapons");

            unclickable.Add(charCat);
            unclickable.Add(weaponCat);

            // Add Character Types
            foreach (Type charType in Character.Characters)
            {
                TreeNode node = new TreeNode($"{charType.Name}s");

                node.NodeFont = fontItalic;
                charCat.Nodes.Add(node);
                unclickable.Add(node);
            }

            // Populate character tree
            foreach(CharacterRow row in charHandler.GetCharacters())
            {
                Character character = (Character)row.Object;

                AddCharacter(row);
            }

            // Populate weapon tree
            foreach (WeaponRow row in weapHandler.GetWeapons())
            {
                Weapon weapon = (Weapon)row.Object;

                AddWeapon(row);
            }

            // Expand the TreeView
            treeView.ExpandAll();
        }

        private void AddCharacter(CharacterRow charRow)
        {
            TreeNode charCat = treeView.Nodes[0]; // Character Node

            Character character = (Character)charRow.Object;

            foreach (TreeNode charSubCat in charCat.Nodes)
            {
                if (charSubCat.Text.Equals($"{character.GetCharacterType()}s"))
                {
                    TreeNode node = new TreeNode(character.Name);
                    node.NodeFont = fontRegular;

                    charSubCat.Nodes.Add(node);
                    entities.Add(node, charRow);
                }
            }
        }

        private void AddWeapon(WeaponRow weapRow)
        {
            TreeNode weapCat = treeView.Nodes[1];

            Weapon weapon = (Weapon)weapRow.Object;

            TreeNode node = new TreeNode($"{weapon.Name} aka '{weapon.Nickname}'");
            node.NodeFont = fontRegular;

            weapCat.Nodes.Add(node);
            entities.Add(node, weapRow);
        }
    }
}
