﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using RPG_Character;

namespace RPG_Character_UI
{
    public class UI_Attributes
    {
        private MainForm form;
        private CreationForm createForm;

        // Database access
        private CharacterHandler charHandler;
        private WeaponHandler weapHandler;

        // Toolbar
        private ToolStripButton newEntityButton;
        private ToolStripButton deleteEntityButton;
        private ToolStripButton updateEntityButton;
        private ToolStripButton duplicateEntityButton;

        // Bottom Toolbar
        private ToolStripLabel statusLabel;

        // Character Attributes
        private TextBox characterName;
        private ComboBox characterTypeInput;
        private NumericUpDown characterHealthInput;
        private NumericUpDown characterArmorInput;
        private NumericUpDown characterEnergyInput;
        private NumericUpDown characterManaInput;

        // Weapon Attributes
        private TextBox weaponName;
        private ComboBox weaponType;
        private TextBox weaponNickname;
        private NumericUpDown weaponFireRate;
        private NumericUpDown weaponRange;
        private NumericUpDown weaponClipSize;
        private NumericUpDown weaponMaxAmmo;
        private NumericUpDown weaponDmgPerHit;

        // Selected entity
        private DBRow selected = null;
        private DBRow altered = null;

        // Events
        public event EventHandler onEntityCreate;
        public event EventHandler onEntityUpdate;
        public event EventHandler onEntityDelete;
        public event EventHandler onEntityDuplicate;

        protected virtual void OnEntityCreated(EventArgs e)
        {
            EventHandler handler = onEntityCreate;
            handler?.Invoke(this, e);
        }

        protected virtual void OnEntityUpdated(EventArgs e)
        {
            EventHandler handler = onEntityUpdate;
            handler?.Invoke(this, e);
        }

        protected virtual void OnEntityDeleted(EventArgs e)
        {
            EventHandler handler = onEntityDelete;
            handler?.Invoke(this, e);
        }

        protected virtual void OnEntityDuplicated(EventArgs e)
        {
            EventHandler handler = onEntityDuplicate;
            handler?.Invoke(this, e);
        }

        public UI_Attributes(MainForm form, CharacterHandler charHandler, WeaponHandler weapHandler)
        {
            this.form = form;
            this.charHandler = charHandler;
            this.weapHandler = weapHandler;

            // Toolbar buttons
            newEntityButton = form.createEntityButton;
            deleteEntityButton = form.deleteButton;
            updateEntityButton = form.updateButton;
            duplicateEntityButton = form.duplicateButton;

            // Bottom Toolbar
            statusLabel = form.statusLabel;

            // Character Input Fields
            characterName = form.charNameBox;
            characterTypeInput = form.charTypeBox;
            characterHealthInput = form.charHPBox;
            characterArmorInput = form.charArmorBox;
            characterEnergyInput = form.charEnergyBox;
            characterManaInput = form.charManaBox;

            // Weapon Input Fields
            weaponName = form.weapNameBox;
            weaponType = form.weapTypeBox;
            weaponNickname = form.weapNickBox;
            weaponFireRate = form.weapRateBox;
            weaponRange = form.weapRangeBox;
            weaponClipSize = form.weapClipBox;
            weaponMaxAmmo = form.weapMaxAmmoBox;
            weaponDmgPerHit = form.weapDmgPerHitBox;

            // Populate Comboboxes
            foreach (Type type in Character.Characters)
                characterTypeInput.Items.Add(type.Name);

            foreach (Type type in Weapon.DefinedWeapons)
                weaponType.Items.Add(type.Name);

            // Bind events
            newEntityButton.Click += onNewEntityClick;
            updateEntityButton.Click += onUpdateClick;
            deleteEntityButton.Click += onDeleteClick;
            duplicateEntityButton.Click += onDuplicateClick;

            // Bind our creation forms events
            createForm = new CreationForm();
            createForm.onEntityCreate += onEntityStartCreation;

            // Bind attribute inputs
            BindCharacterInput();
            BindWeaponInput();
        }

        private void onNewEntityClick(object sender, EventArgs e)
        {
            createForm.Show();
        }

        public void ClearUI()
        {
            // Clear Character Attributes
            characterName.Clear();
            characterHealthInput.Value = 0;
            characterArmorInput.Value = 0;
            characterEnergyInput.Value = 0;
            characterManaInput.Value = 0;

            // Clear Weapon Attributes
            weaponName.Clear();
            weaponNickname.Clear();
            weaponFireRate.Value = 0;
            weaponRange.Value = 0;
            weaponClipSize.Value = 0;
            weaponMaxAmmo.Value = 0;
            weaponDmgPerHit.Value = 0;

            // Revert button names
            deleteEntityButton.Text = "Delete Entity";
            updateEntityButton.Text = "Update Entity";

            // Hide groups
            form.charGroup.Hide();
            form.weapGroup.Hide();

            selected = null;
            altered = null;

            newEntityButton.Enabled = true;
            deleteEntityButton.Enabled = false;
            updateEntityButton.Enabled = false;
            duplicateEntityButton.Enabled = false;
        }

        public void SelectEntity(DBRow row)
        {
            if (row == null) // Deselection
            {
                selected = null;
                ClearUI();
                return;
            }

            if (ChangesMade() && MessageBox.Show("You have attempted to view another entity, but you're currently editing another entity. Proceeding will result in the loss of your current changes, do you wish to save the changes before you proceed?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                onUpdateClick(altered, null);
                MessageBox.Show("Your previous changes have been saved.");
            }

            ClearUI();
            selected = row;

            newEntityButton.Enabled = false;
            deleteEntityButton.Enabled = true;
            updateEntityButton.Enabled = true;
            duplicateEntityButton.Enabled = true;

            switch(row.GetRowType())
            {
                case "CharacterRow":
                    Character character = row.Object as Character;
                    Character charClone = character.Clone();

                    altered = new CharacterRow(row.Id, charClone);

                    ListCharacterAttribute(character);
                    deleteEntityButton.Text = "Delete Character";
                    updateEntityButton.Text = "Update Character";

                    statusLabel.Text = $"Selected character '{character.Name}'";
                    break;
                case "WeaponRow":
                    Weapon weapon = row.Object as Weapon;
                    Weapon weapClone = weapon.Clone();

                    altered = new WeaponRow(row.Id, weapClone);

                    ListWeaponAttribute(weapon);
                    deleteEntityButton.Text = "Delete Weapon";
                    updateEntityButton.Text = "Update Weapon";

                    statusLabel.Text = $"Selected weapon '{weapon.Name}'";
                    break;
            }
        }

        public void ListCharacterAttribute(Character character)
        {
            characterName.Text = character.Name;
            characterHealthInput.Value = character.Health;
            characterArmorInput.Value = character.Armor;
            characterEnergyInput.Value = character.Energy;
            characterManaInput.Value = character.Mana;

            form.charGroup.Show();
        }

        public void ListWeaponAttribute(Weapon weapon)
        {
            weaponName.Text = weapon.Name;
            weaponNickname.Text = weapon.Nickname;
            weaponFireRate.Value = weapon.FireRate;
            weaponRange.Value = weapon.Range;
            weaponClipSize.Value = weapon.ClipSize;
            weaponMaxAmmo.Value = weapon.MaxAmmo;
            weaponDmgPerHit.Value = weapon.DamagePerHit;

            form.weapGroup.Show();
        }

        public bool ChangesMade()
        {
            if (selected == null || altered == null)
                return false;

            bool changed = false;

            switch (selected.GetRowType())
            {
                case "CharacterRow":
                    Character charA = selected.Object as Character;
                    Character charB = altered.Object as Character;

                    if (!charA.Equals(charB))
                        changed = true;

                    break;
                case "WeaponRow":
                    Weapon weapA = selected.Object as Weapon;
                    Weapon weapB = altered.Object as Weapon;

                    if (!weapA.Equals(weapB))
                        changed = true;

                    break;
            }

            return changed;
        }

        private void onEntityStartCreation(object sender, EventArgs e)
        {
            object[] data = sender as object[];
            Type entityType = data[0] as Type;
            string entityName = data[1] as string;

            DBRow row = null;
            if (Character.Characters.Contains(entityType))
            {
                Character entity = Activator.CreateInstance(entityType, entityName) as Character;
                row = new CharacterRow(0, entity);
                charHandler.CreateCharacter(row as CharacterRow);
                MessageBox.Show($"Successfully created a new character.\n\n{entity.ToString()}");
                onEntityCreate(row, null);
            }
            else if (Weapon.DefinedWeapons.Contains(entityType))
            {
                Weapon entity = Activator.CreateInstance(entityType, entityName) as Weapon;
                row = new WeaponRow(0, entity);
                weapHandler.CreateWeapon(row as WeaponRow);
                MessageBox.Show($"Successfully created a new character.\n\n{entity.ToString()}");
                onEntityCreate(row, null);
            }
        }


        private void onUpdateClick(object sender, EventArgs e)
        {
            if (!ChangesMade())
            {
                MessageBox.Show($"Unable to update entity. No changes have been made.");
                return;
            }

            switch (selected.GetRowType())
            {
                case "CharacterRow":
                    Character charB = altered.Object as Character;

                    charHandler.UpdateCharacter(altered as CharacterRow);
                    selected = new CharacterRow(selected.Id, charB.Clone());

                    break;
                case "WeaponRow":
                    Weapon weapB = altered.Object as Weapon;

                    weapHandler.UpdateWeapon(altered as WeaponRow);
                    selected = new WeaponRow(selected.Id, weapB.Clone());
                    break;
            }

            onEntityUpdate(altered, null);
        }

        private void onDeleteClick(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you wish to delete the selected entity?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                return;

            switch (selected.GetRowType())
            {
                case "CharacterRow":
                    charHandler.DeleteCharacter(selected as CharacterRow);

                    break;
                case "WeaponRow":
                    weapHandler.DeleteWeapon(selected as WeaponRow);
                    break;
            }

            onEntityDelete(selected, null);
        }

        private void onDuplicateClick(object sender, EventArgs e)
        {
            if (ChangesMade())
            {
                MessageBox.Show("The duplicate row will possess all information you have altered.", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            UI_Prompt prompt = new UI_Prompt();
            Tuple<bool, string> result = prompt.Show("Please enter a name for your duplicated entity", "Duplicate Entity");

            if (!result.Item1)
            {
                MessageBox.Show("Duplication cancelled by user request.");
                return;
            }

            if (result.Item2 == string.Empty)
            {
                MessageBox.Show("Cancelling entity duplication, you must enter a name for the duplicate entity.");
                return;
            }

            switch (selected.GetRowType())
            {
                case "CharacterRow":
                    Character charB = altered.Object as Character;
                    charB.Name = result.Item2;

                    CharacterRow cRow = new CharacterRow(0, charB);

                    charHandler.CreateCharacter(cRow);
                    onEntityDuplicate(cRow, null);

                    break;
                case "WeaponRow":
                    Weapon weapB = altered.Object as Weapon;
                    weapB.Name = result.Item2;

                    WeaponRow wRow = new WeaponRow(0, weapB);

                    weapHandler.CreateWeapon(wRow);
                    onEntityDuplicate(wRow, null);
                    break;
            }
        }

        // Input Binding
        private void BindCharacterInput()
        {
            // Bind character name field to object
            characterName.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                string value = (sender as TextBox).Text;
                (altered.Object as Character).Name = value;
            };

            // Bind character health field to object
            characterHealthInput.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Character).Health = value;
            };

            // Bind character armor field to object
            characterArmorInput.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Character).Armor = value;
            };

            // Bind character energy field to object
            characterEnergyInput.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Character).Energy = value;
            };

            // Bind character mana field to object
            characterManaInput.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Character).Mana = value;
            };
        }

        private void BindWeaponInput()
        {
            weaponName.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                string value = (sender as TextBox).Text;
                (altered.Object as Weapon).Name = value;
            };

            weaponNickname.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                string value = (sender as TextBox).Text;
                (altered.Object as Weapon).Nickname = value;
            };

            weaponFireRate.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Weapon).FireRate = value;
            };

            weaponDmgPerHit.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Weapon).DamagePerHit = value;
            };

            weaponRange.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Weapon).Range = value;
            };

            weaponClipSize.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Weapon).ClipSize = value;
            };

            weaponMaxAmmo.TextChanged += (sender, e) =>
            {
                if (altered == null)
                    return;

                int value = Convert.ToInt32((sender as NumericUpDown).Value);
                (altered.Object as Weapon).MaxAmmo = value;
            };
        }
    }
}
