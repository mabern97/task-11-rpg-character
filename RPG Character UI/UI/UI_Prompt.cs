﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPG_Character_UI
{
    public class UI_Prompt
    {
        private string userInput = "";

        public Tuple<bool, string> Show(string text, string caption)
        {
            Form prompt = new Form() {
                Width = 400,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };

            Label textLabel = new Label() { Left = 50, Top = 20, Width = 300, Text = text, TextAlign = ContentAlignment.MiddleCenter };
            TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 300 };
            Button confirmation = new Button() { Text = "OK", Left = 250, Width = 50, Top = 70, DialogResult = DialogResult.OK };
            Button cancellation = new Button() { Text = "Cancel", Left = 300, Width = 50, Top = 70, DialogResult = DialogResult.Cancel };

            textBox.TextChanged += (sender, e) =>
            {
                userInput = (sender as TextBox).Text;
            };
            
            confirmation.Click += (sender, e) => {
                prompt.Close();
            };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(cancellation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;

            return new Tuple<bool, string>(prompt.ShowDialog() == DialogResult.OK, userInput);
        }
    }
}
