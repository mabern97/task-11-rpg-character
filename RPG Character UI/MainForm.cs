﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using RPG_Character; // Our Class Library

namespace RPG_Character_UI
{
    public partial class MainForm : Form
    {
        private string log = string.Empty;

        public SQLHelper database;
        public CharacterHandler charHandler;
        public WeaponHandler weapHandler;

        public UI_Tree treeManager;
        public UI_Attributes attributeManager;

        public List<Control> actions;

        public void AddLogMessage(params string[] messages)
        {
            foreach (string message in messages)
                if (log.Length == 0)
                    log = $"{message}";
                else
                    log += $"\r\n{message}";

            // Clear the log and then append (little hack to stay at the bottom of the log)
            logText.Clear();
            logText.AppendText(log);
        }

        public MainForm()
        {
            InitializeComponent();

            database = new SQLHelper(GlobalConstants.DatabaseConnectionString);

            charHandler = new CharacterHandler(this, database);
            weapHandler = new WeaponHandler(this, database);


            treeManager = new UI_Tree(this, charHandler, weapHandler);
            attributeManager = new UI_Attributes(this, charHandler, weapHandler);

            treeManager.PopulateEntityTree();
            treeManager.onEntityClick += TreeManager_onEntityClick;
            treeManager.onEntityDeselect += TreeManager_onEntityDeselect;

            attributeManager.ClearUI();
            attributeManager.onEntityCreate += AttributeManager_onEntityCreate;
            attributeManager.onEntityUpdate += AttributeManager_onEntityUpdate;
            attributeManager.onEntityDuplicate += AttributeManager_onEntityDuplicate;
            attributeManager.onEntityDelete += AttributeManager_onEntityDelete;

            statusLabel.Text = "Database synchronized";
            AddLogMessage("Welcome to the RPG Entity Manager", "Retrieved all RPG Entities from the database");

            exitToolStripMenuItem.Click += (sender, e) =>
            {
                if (attributeManager.ChangesMade() && MessageBox.Show("The changes you have made to an entity will be lost if you exit. Do you wish to proceed?", "Are you sure you wish to exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
                {
                    return;
                }

                Application.Exit();
            };

            // Export Button Logic
            exportAsCSVToolStripMenuItem1.Click += (sender, e) =>
            {
                Exporter.ExportAll(this, Exporter.ExportType.AsCSV);
                AddLogMessage("Exported all entities to a file as a CSV document");
            };

            exportAsJSONToolStripMenuItem1.Click += (sender, e) =>
            {
                Exporter.ExportAll(this, Exporter.ExportType.AsJSON);
                AddLogMessage("Exported all entities to a file as a JSON document");
            };

            actions = new List<Control>();
        }

        private void GetEntityActions(Entity entity)
        {
            // Remove previous actions
            for (int i = 0; i < actions.Count; i++)
                actions[i].Dispose();
            actions.Clear();

            Type[] interfaces = entity.GetType().GetInterfaces();

            List<MethodInfo> methods = new List<MethodInfo>();
            foreach(Type type in interfaces)
            {
                MethodInfo[] methodArray = type.GetMethods();

                for (int i = 0; i < methodArray.Length; i++)
                    methods.Add(methodArray[i]);
            }

            // Create each action button
            Point basePointForActions = new Point(340, 43);
            for (int i = 0; i < methods.Count; i++)
            {
                MethodInfo method = methods[i];

                Button action = new Button();
                action.Text = method.Name;
                action.Size = new Size(141, 25);
                action.Location = new Point(basePointForActions.X, basePointForActions.Y + (i * 30));
                charGroup.Controls.Add(action);

                action.Click += (sender, e) =>
                {
                    object returnValue = method.Invoke(entity, null);
                    double value = Math.Round(Convert.ToDouble(returnValue), 2);

                    switch(method.Name)
                    {
                        case "Move":
                            AddLogMessage($"{method.Name}: {entity.Name} has moved {value} meters. We don't know where they're heading.");
                            break;
                        case "Attack":
                            AddLogMessage($"{method.Name}: {entity.Name} attacked a random pedestrian and took {value} HP away from them.");
                            break;
                        case "Steal":
                            AddLogMessage($"{method.Name}: {entity.Name} stole ${value} from a random pedestrian. Some one please call the police..");
                            break;
                        case "Burgle":
                            AddLogMessage($"{method.Name}: {entity.Name} broke into a nearby property and got away with stealing {value} items");
                            break;
                    }
                };

                actions.Add(action);
            }
        }

        /*
         * Once an entity is created, refresh the tree
         */
        private void AttributeManager_onEntityCreate(object sender, EventArgs e)
        {
            DBRow row = sender as DBRow;
            Entity entity = row.Object as Entity;

            treeManager.PopulateEntityTree();
            treeManager.Select(treeManager.FindNode(entity.Name));
            statusLabel.Text = "Database updated (Created entity)";
            AddLogMessage($"Created database entry for '{entity.Name}'");
        }

        /*
         * Refresh the tree, once an entity is duplicated
         */
        private void AttributeManager_onEntityDuplicate(object sender, EventArgs e)
        {
            treeManager.PopulateEntityTree();
            statusLabel.Text = "Database updated (Duplicated entity)";
            AddLogMessage($"Inserted duplicate entity entry to the database");

            attributeManager.SelectEntity(null);
        }

        /*
         * Refresh the tree, once an entity is updated
         */
        private void AttributeManager_onEntityUpdate(object sender, EventArgs e)
        {
            treeManager.PopulateEntityTree();
            statusLabel.Text = "Database updated (Updated entity)";
            AddLogMessage($"Updated an entity entry from the database");
            attributeManager.SelectEntity(null);
        }

        /*
         * Refresh the tree once an entity is deleted
         */
        private void AttributeManager_onEntityDelete(object sender, EventArgs e)
        {
            treeManager.PopulateEntityTree();
            attributeManager.SelectEntity(null);
            statusLabel.Text = "Database updated (Deleted entity)";
            AddLogMessage($"Deleted an entity entry from the database");
        }

        /*
         * Clear the viewer, once an entity is deselected
         */
        private void TreeManager_onEntityDeselect(object sender, EventArgs e)
        {
            attributeManager.SelectEntity(null);
        }

        /*
         * Once a node in the tree is selected
         * show the details of the node (one of our entities)
         */
        private void TreeManager_onEntityClick(object sender, EventArgs e)
        {
            DBRow row = sender as DBRow;
            attributeManager.SelectEntity(row);

            Entity en = row.Object as Entity;

            GetEntityActions(en);
        }
    }
}
