﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

using RPG_Character;
using System.Diagnostics;
using RPG_Character.Helpers.CSV;
using System.CodeDom;
using System.Linq;

namespace RPG_Character_UI
{
    public class Exporter
    {
        public enum ExportType
        {
            AsCSV,
            AsJSON
        };

        public static void ExportAll(MainForm form, ExportType asType)
        {
            List<CharacterRow> characterRows = form.charHandler.GetCharacters();
            List<WeaponRow> weaponRows = form.weapHandler.GetWeapons();

            List<Character> characters = (from character in characterRows
                                          select (character.Object as Character)).ToList();

            List<Weapon> weapons = (from weapon in weaponRows
                                          select (weapon.Object as Weapon)).ToList();

            Export<Character>(asType, "Select an export location for characters.", "characterExport", characters);
            string directory = Export<Weapon>(asType, "Select an export location for weapons", "eaponExport", weapons);

            if (MessageBox.Show("Would you like to open the directory where the exported content was saved?", "Question", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // Open the directory where the exported file was saved
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    Arguments = directory,
                    FileName = "explorer.exe"
                };
                Process.Start(startInfo);
            }
        }

        public static string Export<T>(ExportType asType, string title, string filename, List<T> data)
        {
            string baseDirectory = Directory.GetCurrentDirectory() + "\\ExportedData";

            if (!Directory.Exists(baseDirectory))
            {
                Directory.CreateDirectory(baseDirectory);
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = title;
            dialog.FileName = filename;
            dialog.InitialDirectory = baseDirectory;
            
            switch(asType)
            {
                case ExportType.AsCSV:
                    dialog.Filter = "CSV File (*.csv)|*.csv";
                    break;
                case ExportType.AsJSON:
                    dialog.Filter = "JSON File (*.json)|*.json";
                    break;
            }
            
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                switch (asType)
                {
                    case ExportType.AsCSV:
                        CSVHelper.ExportObject(dialog.FileName, data);
                        break;
                    case ExportType.AsJSON:
                        JSONHelper.ExportObject(dialog.FileName, data);
                        break;
                }
            }

            return baseDirectory;
        }
    }
}
