USE [master]
GO
/****** Object:  Database [RPGDB]    Script Date: 8/26/2020 2:07:40 PM ******/
CREATE DATABASE [RPGDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RPGDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RPGDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RPGDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RPGDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [RPGDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RPGDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RPGDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RPGDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RPGDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RPGDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RPGDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [RPGDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RPGDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RPGDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RPGDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RPGDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RPGDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RPGDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RPGDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RPGDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RPGDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RPGDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RPGDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RPGDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RPGDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RPGDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RPGDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RPGDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RPGDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RPGDB] SET  MULTI_USER 
GO
ALTER DATABASE [RPGDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RPGDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RPGDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RPGDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RPGDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RPGDB] SET QUERY_STORE = OFF
GO
USE [RPGDB]
GO
/****** Object:  Table [dbo].[Characters]    Script Date: 8/26/2020 2:07:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Characters](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Health] [int] NOT NULL,
	[Armor] [int] NOT NULL,
	[Energy] [int] NOT NULL,
	[Mana] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Weapons]    Script Date: 8/26/2020 2:07:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Weapons](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Nickname] [nvarchar](50) NULL,
	[FireRate] [int] NOT NULL,
	[DamagePerHit] [int] NOT NULL,
	[Range] [int] NOT NULL,
	[ClipSize] [int] NOT NULL,
	[MaxAmmo] [int] NOT NULL,
	[CompatibleCharacters] [nvarchar](50) NULL,
 CONSTRAINT [PK_Weapons] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Characters] ON 

INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (11, N'Nicholas Lennox', N'Warrior', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (12, N'Dewald Els', N'Wizard', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (13, N'Nicolas Anderson2', N'Thief', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (15, N'Harry Potters', N'Mage', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (16, N'Ron Weasly', N'Mage', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (17, N'Mathias T. Berntsen', N'Thief', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (18, N'Elon Musk', N'Warrior', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (21, N'Kenny', N'Thief', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (24, N'Nicholas Lennox', N'Gangster', 100, 100, 100, 100)
INSERT [dbo].[Characters] ([ID], [Name], [Type], [Health], [Armor], [Energy], [Mana]) VALUES (25, N'Dewald Els', N'Gangster', 100, 100, 100, 100)
SET IDENTITY_INSERT [dbo].[Characters] OFF
GO
SET IDENTITY_INSERT [dbo].[Weapons] ON 

INSERT [dbo].[Weapons] ([ID], [Type], [Name], [Nickname], [FireRate], [DamagePerHit], [Range], [ClipSize], [MaxAmmo], [CompatibleCharacters]) VALUES (4, N'Mystical', N'Magical Wand', N'Hairy''o Potter Wand', 15, 20, 5, 20, 150, NULL)
INSERT [dbo].[Weapons] ([ID], [Type], [Name], [Nickname], [FireRate], [DamagePerHit], [Range], [ClipSize], [MaxAmmo], [CompatibleCharacters]) VALUES (5, N'SemiAutomatic', N'Uzi', N'Uzi', 15, 20, 5, 20, 150, NULL)
INSERT [dbo].[Weapons] ([ID], [Type], [Name], [Nickname], [FireRate], [DamagePerHit], [Range], [ClipSize], [MaxAmmo], [CompatibleCharacters]) VALUES (6, N'Rifle', N'AK-47', N'AK-47', 50, 20, 5, 20, 150, NULL)
INSERT [dbo].[Weapons] ([ID], [Type], [Name], [Nickname], [FireRate], [DamagePerHit], [Range], [ClipSize], [MaxAmmo], [CompatibleCharacters]) VALUES (7, N'Shotgun', N'SPAS-12', N'SPAS-12', 15, 20, 5, 20, 150, NULL)
INSERT [dbo].[Weapons] ([ID], [Type], [Name], [Nickname], [FireRate], [DamagePerHit], [Range], [ClipSize], [MaxAmmo], [CompatibleCharacters]) VALUES (8, N'SemiAutomatic', N'M16', N'M16', 15, 20, 5, 20, 150, NULL)
INSERT [dbo].[Weapons] ([ID], [Type], [Name], [Nickname], [FireRate], [DamagePerHit], [Range], [ClipSize], [MaxAmmo], [CompatibleCharacters]) VALUES (9, N'Shotgun', N'Ranger', N'Chicken Plucker', 15, 20, 5, 20, 150, NULL)
SET IDENTITY_INSERT [dbo].[Weapons] OFF
GO
USE [master]
GO
ALTER DATABASE [RPGDB] SET  READ_WRITE 
GO
